document.addEventListener("DOMContentLoaded", function() {
	init();
}, false);


function init() {

	var update = document.querySelectorAll(".button_update");
    for (var i = 0; i < update.length; i++) {
        update[i].addEventListener("click", updateResults);
    }

	//var text2 = config.admin_url;
    api_url = config.api_url;
    console.log(api_url);
}

function updateResults() {
	fetchAPI(this);
}
function displayResults(button, data) {
	var field_name = button.getAttribute("data-destination");
	var textarea = document.querySelector("textarea[name='"+field_name+"']");
	// console.log(JSON.stringify(data));
	textarea.value = data;
}

function fetchAPI(button) {
    // var url = "https://vincent-maillard.fr/projets/performance_lab/omeka_coll_sites_savoirs/api/items";
    var api = api_url;

    var field_name = button.getAttribute("data-field");
    var current_val = document.querySelector("input[name='"+field_name+"']").value;

    // var url = api + "items/" + current_val;
     var url = api + current_val;
    console.log(url);

    var myHeaders = new Headers();
    myHeaders.append('Content-Type','text/plain; charset=UTF-8');

    fetch(url, myHeaders)
        .then(function(response) { 
            return response.text();
        })
        .then(function(text) {
            console.log(text);
         	// console.log(JSON.parse(text));
        	displayResults(button, text);
        })
}
